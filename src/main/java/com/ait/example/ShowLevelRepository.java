package com.ait.example;

import com.google.common.base.Predicate;

@org.springframework.stereotype.Repository
public class ShowLevelRepository extends Repository<Showlevel> {

    public Showlevel findByDescription(final String categoryDescription) {
        return find(new Predicate<Showlevel>() {
            public boolean apply(Showlevel gameCategory) {
                return gameCategory.getDescription().equals(categoryDescription);
            }
        });
    }

}
