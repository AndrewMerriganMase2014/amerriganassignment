package com.ait.example;

import com.google.common.base.Predicate;

@org.springframework.stereotype.Repository
public class ShowRepository extends Repository<Show> {

    public Show findByName(final String opponent) {
        return find(new Predicate<Show>() {
            public boolean apply(Show game) {
                return game.getName().equals(opponent);
            }
        });
    }
}
